from analysis_tools import ObjectCollection, Category, Process, Dataset, Feature, Systematic
from analysis_tools.utils import DotDict
from analysis_tools.utils import join_root_selection as jrs
from plotting_tools import Label
from collections import OrderedDict

from config.run3_base_config import Config as base_config
from config.run3_dataset_2023_preBPix import Datasets_2023_preBPix as dataset_config

class Config_2023_preBPix(base_config, dataset_config):
    def __init__(self, *args, **kwargs):
        super(Config_2023_preBPix, self).__init__(*args, **kwargs)

    def add_weights(self):
        weights = DotDict()
        weights.default = "1"
        weights.total_events_weights = ["genWeight", "puWeight"]

        weights.corrections = ["genWeight", "puWeight", "idAndIsoAndFakeSF"] #, "btagsf_shape"]

        weights.base = weights.corrections
        weights.baseline = weights.corrections
        weights.base_selection = weights.corrections

        weights.mutau = weights.corrections
        weights.etau = weights.corrections
        weights.tautau = weights.corrections

        return weights

    def add_default_module_files(self):
        defaults = {}
        defaults["PreprocessRDF"] = "run3_modulesrdf"
        defaults["PreCounter"] = "run3_weights"
        return defaults

config = Config_2023_preBPix("Config_2023_preBPix", year=2023, runPeriod="preBPix", ecm=13.6, lumi_pb=9516)