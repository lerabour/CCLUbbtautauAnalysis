# HHbbtautau-analysis
This is the main configuration code of the HH->bbtautau search (easily extendable to ZZ/ZH->bbtautau).
The analysis is built around the the NanoAOD-base-analysis [main code](https://gitlab.cern.ch/cms-phys-ciemat/nanoaod_base_analysis.git) developped at CIEMAT.

Make a fork of this repository, so you can use it as the basis for your own analysis.

## HHbbtautau spreadsheets
Status : https://docs.google.com/spreadsheets/d/1vR2GOqDIn1qMZUHOupDVaWxKxAjxwO7ctVUYCJKa630/edit#gid=0

Datasets : https://docs.google.com/spreadsheets/d/1PUEihTEB0PxiJJoCxsJXM1eOXV98TW3sq0fjiXAppec/edit#gid=0

## Installation instructions
```shell
git clone ssh://git@gitlab.cern.ch:7999/cclubbtautau/CCLUbbtautauAnalysis.git
cd CCLUbbtautauAnalysis
source setup.sh
law index --verbose #to do only after installation or including a new task
```
Druing installation the setup will automatically pull all required packages from varios GitLab and GitHub projects, and will install all required software.

## User guide:
Information about the code, how to install it, setting a configuration to use it and more useful information about this framework can also be found [here](https://nanoaod-base-analysis.readthedocs.io).

Two walkthrough presentations with detailed information on how to run the workflow can be found [here](https://cernbox.cern.ch/s/nsOEwYQwezh1ZMI)

### Running on lxplus7

As it is, NBA does not support condor job submission on lxplus7. A simple fix is required to make it work:

- In `cms/base_tasks/base.py`, add on [l.346](https://gitlab.cern.ch/cms-phys-ciemat/nanoaod_base_analysis/-/blob/py3/cmt/base_tasks/base.py?ref_type=heads#L346):
```
config.custom_content.append(("MY.WantOS", '"el7"'))
```

- In `cms/files/cern_htcondor_bootstrap.sh`, replace
```
source "{{cmt_base}}/../setup.sh" ""
```
by
```
oldpwd=$PWD
cd "{{cms_base}}/../"
source setup.sh ""
cd $oldpwd
```

## Structure of the CCLUbbtautau group

### CCLUbbtautau group
Contains the HH/ZH/ZZ->bbtautau analyses higher-level configuration code
* HHbbtautau : contains the analysis environment and configuration
* AnalysisCore :contains the analysis core code

Basic idea and practices:
* central code for the HHbbtautau analysis
* all users should fork these two for their own usage
* all modifications PR'ed and validated in here

### CCLUbbtautau/NBA-fw-synch` subgroup
Contains all the forks useful to the synchronisation with the central NBA framework
* analysis_tools
* btv-corrections
* cmt-base-modules
* egm-corrections
* event-filters
* jme-corrections
* lum-corrections
* muo-corrections
* nanoaod_base_analysis
* plotting_tools
* tau-corrections

Basic idea and practices:
* forks of all cms-phys-ciemat folders
* everyone can do direct push here!!! be very careful!!! (the main issue I see is for the NBA fork itself, but changes would generally be minor)
* all modifications PR'ed to cms-phys-ciemat and validated there
