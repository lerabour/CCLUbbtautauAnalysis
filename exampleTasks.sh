# 0 - Common arguments:
MY_VERSION="2023triggerStudy"
MY_CONFIG="run3_2023_preBPix"
MY_DATASET="GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00"
MY_NWORKERS="10"
MY_BASECATEGORY="base"
MY_CATEGORY="baseline"
MY_REGION="tautau_os_iso"
MY_PLOTVAR="lep1_pt,lep1_eta,lep1_phi,lep2_pt,lep2_eta,lep2_phi,genHH_mass,Htt_pt,Htt_eta,Htt_phi,Htt_mass"

# 1 - PreCounter 
#   -> Reads input nanoaod samples, list files & statistics in each of them 
#   -> Run once per sample
law run PreCounter \
    --version $MY_VERSION \
    --config-name $MY_CONFIG \
    --dataset-name $MY_DATASET \
    --workers $MY_NWORKERS

# 2 - MergeCategorizationStats
#   -> Evaluate overall statistics for each sample (TBC)
#   -> run once per sample
law run MergeCategorizationStats \
    --version $MY_VERSION \
    --config-name $MY_CONFIG \
    --dataset-name $MY_DATASET \
    --workers $MY_NWORKERS

# 3 - PreprocessRDF (/!\ longest step (?) to be done on batch system or on very small samples)
#   -> Main event loop, applies first loose object selection 
#   -> run once per sample
law run PreprocessRDF \
    --version $MY_VERSION \
    --config-name $MY_CONFIG \
    --dataset-name $MY_DATASET \
    --workers $MY_NWORKERS \
    --category-name $MY_BASECATEGORY \
    --keep-and-drop-file run3_keep_and_drop_file

# 4 - Categorization
#   -> saves skimmed TTrees with only events in category
#   -> run once per sample per category (baseline, res1b, res2b, boosted, VBF) ?
law run Categorization \
    --version $MY_VERSION \
    --config-name $MY_CONFIG \
    --dataset-name $MY_DATASET \
    --workers $MY_NWORKERS \
    --category-name $MY_CATEGORY \
    --base-category-name $MY_BASECATEGORY \
    --keep-and-drop-file run3_keep_and_drop_file

# 5 - MergeCategorization
#   -> Merge individual ROOT files into one for each sample/category 
#   -> run once per sample per category (baseline, res1b, res2b, boosted, VBF) ?
law run MergeCategorization \
    --version $MY_VERSION \
    --config-name $MY_CONFIG \
    --dataset-name $MY_DATASET \
    --workers $MY_NWORKERS \
    --category-name $MY_CATEGORY \
    --Categorization-modules-file run3_modulesrdf2023

# 6 - PrePlot
#   -> Generate histograms & save them in ROOT file
#   -> runs once per sample per category (baseline, res1b, res2b, boosted, VBF) per channel (etau,mutau,tautau,...) per region (os_iso,ss_iso,os_inviso,ss_inviso)?
law run PrePlot \
    --version $MY_VERSION \
    --config-name $MY_CONFIG \
    --dataset-name $MY_DATASET \
    --workers $MY_NWORKERS \
    --category-name $MY_CATEGORY \
    --Categorization-base-category-name $MY_BASECATEGORY \
    --region-name $MY_REGION \
    --feature-names $MY_PLOTVAR

# 7 - FeaturePlot
#   -> Draw histograms into pdf
#   -> runs once per sample per category (baseline, res1b, res2b, boosted, VBF) per channel (etau,mutau,tautau,...) per region (os_iso,ss_iso,os_inviso,ss_inviso)?
law run FeaturePlot \
    --version $MY_VERSION \
    --config-name $MY_CONFIG \
    --dataset-name $MY_DATASET \
    --workers $MY_NWORKERS \
    --category-name $MY_CATEGORY \
    --region-name $MY_REGION \
    --feature-names $MY_PLOTVAR \
    --process-group-name ggf \
    --store-systematics False \
    --plot-systematics False \
    --save-root \
    --save-pdf \
    --save-yields \
    --stack \
    --hide-data